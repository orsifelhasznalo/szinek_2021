import os, shutil, tempfile, zipfile

def sanitize(s):
    return s.translate(str.maketrans(
        'árvíztűrő tükörfúrógép ÁRVÍZTŰRŐ TÜKÖRFÚRÓGÉP',
        'arvizturo tukorfurogep ARVIZTURO TUKORFUROGEP'
        ))

def main():
    # create file structure
    source_list = ['..', '..', 'Blathy', 'BMSZC Bláthy Ottó Titusz Informatikai Technikum']
    source_path = os.path.join(*source_list)
    subdircount = 0
    for source_dir in os.listdir(source_path):
        if not source_dir.startswith('9.'): continue
        local_dir = source_dir[:3]
        print(local_dir)
        shutil.rmtree(local_dir)
        source_working_dir = os.path.join(source_path, source_dir, 'Working files')
        for remote_student in os.listdir(source_working_dir):
            local_student = sanitize(remote_student)
            student_dir = os.path.join(local_dir, local_student)
            os.makedirs(student_dir, exist_ok=True)
            print(local_student)
            files_copied = 0
            for source_student_dir in [
                os.path.join(source_working_dir, remote_student, 'dolgozat színek'),
                os.path.join(source_working_dir, remote_student, 'Dolgozat - színek'),
                os.path.join(source_working_dir, remote_student, 'dolgozat - színek 2021-12-15'),
            ]:
                for dirpath, dirnames, filenames in os.walk(source_student_dir): # ez szerintem rossz, de működik
                    if dirnames:
                        subdircount += 1
                        print(f'DIR FOUND: {dirnames}')
                    for filename in filenames:
                        if filename.endswith('.html') or filename.endswith('HTML'):
                            shutil.copy(
                                os.path.join(source_student_dir, filename),
                                os.path.join(student_dir, filename)
                            )
                            print(f'    {filename}')
                            files_copied += 1
                        if filename.endswith('.zip'):
                            with tempfile.TemporaryDirectory() as tmpdirname:
                                zip_path = os.path.join(tmpdirname, filename)
                                shutil.copy(os.path.join(source_student_dir, filename), zip_path)
                                with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                                    zip_ref.extractall(tmpdirname)
                                for topdir, subdirs, files in os.walk(tmpdirname):
                                    for subfilename in files:
                                        if subfilename.endswith('.html') or subfilename.endswith('.HTML'):
                                            html_file = os.path.join(topdir, subfilename)
                                            target_file = f'{filename} - {subfilename}'
                                            target_path = os.path.join(student_dir, target_file)
                                            if os.path.isfile(target_path):
                                                raise Exception(f'file already exists: {target_path}')
                                            shutil.copy(html_file, target_path)
                                            files_copied += 1
                                            print(f'    {subfilename}')
            print(f'copied {files_copied} files')
    if subdircount:
        print(f'found {subdircount} subdirs')

if __name__ == '__main__':
    main()
