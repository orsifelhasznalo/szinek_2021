#!/bin/bash

for class in */; do
    if [ ${class:0:1} != '9' ]; then continue; fi
    echo ---
    echo $class
    cd $class
    for student in */; do
        if [ ${student:0:1} == '.' ]; then continue; fi
        branch=$(echo $class${student:0:-1} | tr ' ' '-')
        echo $branch
        git branch -d $branch
        git checkout -b $branch
        git add "$student"
        git commit -m "$student"
        git push --set-upstream origin $branch --force
        git checkout master
    done
    cd ..
done
